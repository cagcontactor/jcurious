/*
 * Created by Daniel Marell 13-01-27 3:16 PM
 */
package se.caglabs.javafxtest;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;

public class ImageResize extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("HTML");
        stage.setWidth(500);
        stage.setHeight(500);
        Scene scene = new Scene(new Group());
        VBox root = new VBox();

        final ImageView selectedImage = new ImageView();

        Image image1 = new Image(getClass().getResourceAsStream("/daniel.jpg"), 100, 150, false, false);
//        Image image1 = new Image(new File("a.jpg").toURI().toString(), 100, 150, false, false);

        selectedImage.setImage(image1);

        root.getChildren().addAll(selectedImage);

        scene.setRoot(root);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
