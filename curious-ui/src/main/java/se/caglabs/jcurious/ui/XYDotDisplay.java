/*
 * Created by Daniel Marell 13-01-30 5:37 PM
 */
package se.caglabs.jcurious.ui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Displays a dot in a XY-panel.
 */
public class XYDotDisplay extends Canvas {
    private static final int SI_SZ = 140;
    private static final int SI_DOT_SZ = 10;

    public XYDotDisplay() {
        super(SI_SZ, SI_SZ);
    }

    /**
     * Sets position of the dot. 0,0 is in center.
     *
     * @param x X-coordinate -1..1
     * @param y Y-coordinate -1..1
     */
    public void setDotPosition(double x, double y) {
        GraphicsContext gc = getGraphicsContext2D();
        gc.setFill(Color.valueOf("#efefe0"));
        gc.fillRect(0, 0, SI_SZ, SI_SZ);

        gc.setStroke(Color.rgb(200, 200, 200, 0.8));
        gc.setLineWidth(1);
        gc.strokeLine(0, 0, SI_SZ - 1, SI_SZ - 1);
        gc.strokeLine(0, SI_SZ - 1, SI_SZ - 1, 0);

        gc.setFill(Color.BLUE);
        gc.setStroke(Color.BLUE);
        gc.fillOval(
                x * SI_SZ + SI_SZ / 2 - SI_DOT_SZ / 2,
                y * SI_SZ + SI_SZ / 2 - SI_DOT_SZ / 2,
                SI_DOT_SZ, SI_DOT_SZ);
    }
}

