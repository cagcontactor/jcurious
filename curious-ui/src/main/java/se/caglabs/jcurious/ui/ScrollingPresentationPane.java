/*
 * Created by Daniel Marell 13-02-03 12:13 AM
 */
package se.caglabs.jcurious.ui;

import javafx.animation.Interpolator;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Pane displaying scrolling presentation text and images.
 */
public class ScrollingPresentationPane extends HBox {

    public ScrollingPresentationPane() {
        String text = readFileFromResources("/presentation.txt");

        Text textRef = new Text();
        textRef.setLayoutY(100);
        textRef.setTextOrigin(VPos.TOP);
        textRef.setTextAlignment(TextAlignment.JUSTIFY);
        textRef.setWrappingWidth(450);
        textRef.setText(text);
        textRef.setFill(Color.valueOf("#1f1f10"));
        textRef.setEffect(new Glow(0.8));
        textRef.setFont(javafx.scene.text.Font.font("SansSerif", FontWeight.BOLD, 24));

        VBox vpane = new VBox();
        vpane.getChildren().add(textRef);
        Image danielImageSmall = new Image(getClass().getResourceAsStream("/daniel.jpg"), 100, 100, true, true);
        vpane.getChildren().add(new ImageView(danielImageSmall));

        // Provides the animated scrolling behavior for the text
        TranslateTransition transTransition = new TranslateTransition();
        transTransition.setDuration(new Duration(75000));
        transTransition.setNode(vpane);
        transTransition.setToY(-800);
        transTransition.setInterpolator(Interpolator.LINEAR);
        transTransition.setCycleCount(Timeline.INDEFINITE);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefWidth(500);
        scrollPane.setPrefHeight(150);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setPannable(true);
        scrollPane.setContent(vpane);
        scrollPane.setStyle("-fx-background-color: transparent;");

        Group pane = new Group();
        pane.getChildren().add(scrollPane);
        getChildren().add(pane);

        transTransition.play();
    }

    private String readFileFromResources(String name) {
        try {
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(name), "UTF-8"));
            for (int c = br.read(); c != -1; c = br.read()) {
                sb.append((char) c);
            }
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot find resource: " + name);
        }
    }
}
