/*
 * Created by Daniel Marell 13-01-23 8:02 AM
 */
package se.caglabs.jcurious.ui;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Listener;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import se.caglabs.jcurious.arm.ArmPose;
import se.caglabs.jcurious.arm.RobotArm;
import se.caglabs.jcurious.arm.ServoFunction;
import se.caglabs.jcurious.brain.OperationMode;
import se.caglabs.jcurious.brain.RobotBrain;
import se.caglabs.jcurious.vision.DetectedObject;
import se.caglabs.jcurious.vision.FaceDetectionGrabber;
import se.caglabs.jcurious.vision.GrabberResult;
import se.caglabs.jcurious.vision.Point;
import se.marell.dcommons.time.PassiveTimer;
import se.marell.dcommons.util.Filter;
import se.marell.dcommons.util.FilterSIIR;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CuriousApp extends Application {
    private RobotArm robotArm;
    private FaceDetectionGrabber grabber;
    private RobotBrain robotBrain;
    private Map<ServoFunction, Slider> sliderMap = new HashMap<>();
    private ToggleGroup modeGroup = new ToggleGroup();
    private ImageView cameraView;
    private XYDotDisplay steerIndicator;
    private List<Button> poseButtons = new ArrayList<>();
    private List<RadioButton> modeButtons = new ArrayList<>();
    private final Filter fpsFilter = new FilterSIIR(0, 3);
    private final Filter cpsFilter = new FilterSIIR(0, 3);
    private Controller controller;
    private Listener listener;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("jcurious");

        String miniSccPortName = getParameters().getNamed().get("miniSccPortName");
        String cameraNumberString = getParameters().getNamed().get("cameraNumber");
        int cameraNumber = 0;
        if (cameraNumberString != null) {
            cameraNumber = Integer.parseInt(cameraNumberString);
        }
        grabber = new FaceDetectionGrabber(cameraNumber);
        robotArm = new RobotArm(miniSccPortName);
        robotBrain = new RobotBrain(robotArm, new SoundPlayerImpl());

        Label sliderLabel = new Label("Servo position");

        HBox poseBox = new HBox();
        poseBox.getStyleClass().add("pose-pane");

        Button coolBtn = createPoseButton("Cool", actionEvent -> {
            robotArm.setPose(ArmPose.cool);
            setSlidersFromServos();
        });
        poseButtons.add(coolBtn);
        poseBox.getChildren().add(coolBtn);

        Button curiousBtn = createPoseButton("Curious", actionEvent -> {
//            robotArm.setPose(ArmPose.curious);
            robotArm.setPose(ArmPose.upright);
            setSlidersFromServos();
        });
        poseButtons.add(curiousBtn);
        poseBox.getChildren().add(curiousBtn);

        Button defensiveBtn = createPoseButton("Upright", actionEvent -> {
            robotArm.setPose(ArmPose.upright);
            setSlidersFromServos();
        });
        poseButtons.add(defensiveBtn);
        poseBox.getChildren().add(defensiveBtn);

        Button laidBackBtn = createPoseButton("Laid back", actionEvent -> {
            robotArm.setPose(ArmPose.laidBack);
            setSlidersFromServos();
        });
        poseButtons.add(laidBackBtn);
        poseBox.getChildren().add(laidBackBtn);


        VBox sliderBox = new VBox();
        sliderBox.getStyleClass().add("sliders-pane");

        final CheckBox manualCheckBox = new CheckBox("Human control");
        manualCheckBox.setOnAction(actionEvent -> {
            robotBrain.setManualMode(manualCheckBox.isSelected());
            setEnableState();
        });
        sliderBox.getChildren().add(manualCheckBox);
        sliderBox.getChildren().add(sliderLabel);
        sliderBox.getChildren().add(createSliderPane("Top tilt", -100, 100, 0, 20, 50, 20, ServoFunction.topTilt));
        sliderBox.getChildren().add(createSliderPane("Middle tilt", 0, 100, 50, 10, 50, 20, ServoFunction.middleTilt));
        sliderBox.getChildren().add(createSliderPane("Base tilt", 0, 100, 50, 10, 50, 20, ServoFunction.baseTilt));
        sliderBox.getChildren().add(createSliderPane("Rotation", -100, 100, 0, 20, 50, 20, ServoFunction.turn));
        sliderBox.getChildren().add(poseBox);

        VBox modeBox = new VBox();
        modeBox.getStyleClass().add("mode-pane");
        boolean first = true;
        for (OperationMode opMode : robotBrain.getOperationModes()) {
            RadioButton btn = createModeButton(opMode);
            if (first) {
                btn.setSelected(true);
                first = false;
            }
            modeButtons.add(btn);
            modeBox.getChildren().add(btn);
        }

        steerIndicator = new XYDotDisplay();
        HBox steerIndicatorPane = new HBox();
        steerIndicatorPane.getStyleClass().add("steer-indicator");
        steerIndicatorPane.getChildren().add(steerIndicator);

        HBox hbox = new HBox();
        hbox.getChildren().addAll(modeBox, steerIndicatorPane);

        VBox controlsBox = new VBox();
        controlsBox.getStyleClass().add("controls-pane");
        controlsBox.getChildren().addAll(sliderBox, hbox);

        BorderPane bottomPane = new BorderPane();
        bottomPane.getStyleClass().add("bottom-pane");
        bottomPane.setLeft(createImagePane("/qr-blog.cag.se.png", "qr-code-view-pane", 100));
        bottomPane.setCenter(createPresentationPane());
        bottomPane.setRight(createImagePane("/CAG green.png", "cag-logo-pane", 200));

        final HBox camerapane = new HBox();

        final BorderPane topPane = new BorderPane();
        topPane.getStyleClass().add("top-pane");
        topPane.setLeft(controlsBox);
        topPane.setCenter(createImageView(camerapane));
        topPane.setBottom(bottomPane);

        setEnableState();

        Scene scene = new Scene(topPane);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        primaryStage.show();

        startControlThread();
        startVisionThread(camerapane);
        startLeapMotion();
    }

    private void startLeapMotion() {
        controller = new Controller();
        listener = new Listener() {
            @Override
            public void onFrame(Controller controller) {
                super.onFrame(controller);
                if (controller.frame().hands().count() > 0) {
                    float angle = controller.frame().hands().get(0).direction().yaw();
                    robotArm.setWantedPosition(ServoFunction.turn, angle);
                }
            }
        };
        controller.addListener(listener);
    }

    private void startVisionThread(HBox camerapane) {
        Thread visionThread = new Thread(() -> {
            PassiveTimer fpsTimer = new PassiveTimer(2000);
            int count = 0;
            while (true) {
                if (fpsTimer.hasExpired()) {
                    fpsTimer.restart();
                    synchronized (fpsFilter) {
                        fpsFilter.in(count / 2);
                    }
                    count = 0;
                }
                ++count;
                try {
                    visionTick(camerapane);
                } catch (FaceDetectionGrabber.FaceDetectionGrabberException e) {
                    System.out.println("getImage exception:" + e.getMessage());
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ignore) {
                }
            }
        });
        visionThread.setDaemon(true);
        visionThread.start();
    }

    private void startControlThread() {
        Thread controlThread = new Thread(() -> {
            PassiveTimer cpsTimer = new PassiveTimer(2000);
            int count = 0;
            while (true) {
                if (cpsTimer.hasExpired()) {
                    cpsTimer.restart();
                    synchronized (cpsFilter) {
                        cpsFilter.in(count / 2);
                    }
                    count = 0;
                }
                ++count;
                controlTick();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ignore) {
                }
            }
        });
        controlThread.setDaemon(true);
        controlThread.start();
    }

    private void visionTick(final HBox camerapane) throws FaceDetectionGrabber.FaceDetectionGrabberException {
        GrabberResult gr = grabber.getGrabberResult();
        final BufferedImage grabbedImage = gr.getOriginalImage();
        robotBrain.setDetectedFaces(gr.getDetectedObjects(), grabbedImage.getWidth(), grabbedImage.getHeight());
        robotBrain.tick();
        Platform.runLater(() -> {
            steerIndicator.setDotPosition(robotBrain.getArmControlSignal().getX(), robotBrain.getArmControlSignal().getY());
            drawFaces(grabbedImage, robotBrain.getAllFaces());
            drawRightInfoText(grabbedImage, String.format("FPS: %.0f CPS: %.0f", fpsFilter.out(), cpsFilter.out()));
            drawLeftInfoText(grabbedImage, robotBrain.getInfoString());
            Point facePoint = robotBrain.getFaceFocusCenterPoint();
            if (facePoint != null) {
                Point drawPoint = new Point(
                        Math.round(facePoint.getX() + grabbedImage.getWidth() / 2),
                        Math.round(facePoint.getY() + grabbedImage.getHeight() / 2));
                drawFacePoint(grabbedImage, drawPoint);
            }
            if (camerapane.getWidth() > 0) {
                BufferedImage image = resizeImageToPane(grabbedImage, camerapane);
                setImage(image);
            }
        });
    }

    private void controlTick() {
        robotArm.tick();
        if (!robotBrain.isManualMode()) {
            setSlidersFromServos();
        }
    }

    private void setEnableState() {
        for (Slider s : sliderMap.values()) {
            s.setDisable(!robotBrain.isManualMode());
        }
        for (Button b : poseButtons) {
            b.setDisable(!robotBrain.isManualMode());
        }
        for (RadioButton b : modeButtons) {
            b.setDisable(robotBrain.isManualMode());
        }
    }

    private void drawFacePoint(BufferedImage img, Point point) {
        Graphics2D g = img.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.setColor(java.awt.Color.red);
        g.drawOval(point.getX() - 10, point.getY() - 10, 20, 20);
        g.dispose();
    }

    private void drawFaces(BufferedImage img, List<DetectedObject> faces) {
        Graphics2D g = img.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        // Draw white rectangle
        g.setColor(java.awt.Color.white);
        g.setStroke(new BasicStroke(3));
        for (DetectedObject face : faces) {
            int x = face.getBox().getX();
            int y = face.getBox().getY();
            int w = face.getBox().getWidth();
            int h = face.getBox().getHeight();
            g.drawRect(x, y, w, h);
        }

        // Draw green hat
        g.setColor(java.awt.Color.green);
        for (DetectedObject face : faces) {
            int x = face.getBox().getX();
            int y = face.getBox().getY();
            int w = face.getBox().getWidth();
            int h = face.getBox().getHeight();
            g.fillPolygon(
                    new int[]{x - w / 10, x + w * 11 / 10, x + w / 2},
                    new int[]{y - h / 10, y - h / 10, y - h / 2},
                    3);
        }
        g.dispose();
    }

    private void drawRightInfoText(BufferedImage img, String text) {
        Graphics2D g = img.createGraphics();
        g.setColor(Color.GREEN);
        g.drawString(text, img.getWidth() - 120, img.getHeight() - 20);
        g.dispose();
    }

    private void drawLeftInfoText(BufferedImage img, String text) {
        Graphics2D g = img.createGraphics();
        g.setColor(Color.GREEN);
        g.drawString(text, 20, img.getHeight() - 20);
        g.dispose();
    }

    private BufferedImage resizeImageToPane(BufferedImage image, HBox camerapane) {
        double paneWidth = Math.round(camerapane.getWidth() - camerapane.getPadding().getLeft() - camerapane.getPadding().getRight() - camerapane.getInsets().getLeft() - camerapane.getInsets().getRight());
        double paneHeight = Math.round(camerapane.getHeight() - camerapane.getPadding().getTop() - camerapane.getPadding().getBottom() - camerapane.getInsets().getTop() - camerapane.getInsets().getBottom());
        double origWidth = image.getWidth();
        double origHeight = image.getHeight();
        double newWidth = paneWidth;
        double newHeight = origHeight / origWidth * paneWidth;
        if (newHeight > paneHeight) {
            newHeight = paneHeight;
            newWidth = origWidth / origHeight * paneHeight;
        }
        image = resize(image, (int) Math.round(newWidth), (int) Math.round(newHeight));
        return image;
    }

    private Node createPresentationPane() {
        BorderPane pane = new BorderPane();
        pane.getStyleClass().add("presentation-pane");
        pane.setCenter(new ScrollingPresentationPane());
        return pane;
    }

    private Node createImagePane(String resourceName, String styleClassName, int fitWidth) {
        final Image image = new Image(getClass().getResourceAsStream(resourceName));
        ImageView view = new ImageView(image);
        if (fitWidth > 0) {
            view.setFitWidth(fitWidth);
        }
        view.setPreserveRatio(true);
        view.setSmooth(true);
        view.setCache(true);
        HBox box = new HBox();
        box.getStyleClass().add(styleClassName);
        box.getChildren().add(view);
        return box;
    }

    private Node createImageView(HBox camerapane) {
        cameraView = new ImageView(new Image(getClass().getResourceAsStream("/daniel.jpg")));
        camerapane.getStyleClass().add("camera-pane");
        camerapane.getChildren().add(cameraView);
        camerapane.setPrefWidth(280);
        camerapane.setMinWidth(100);
        camerapane.setMinHeight(100);
        return camerapane;
    }

    private void setImage(final BufferedImage bufferedImage) {
        if (cameraView != null) {
            final WritableImage image = new WritableImage(bufferedImage.getWidth(), bufferedImage.getHeight());
            SwingFXUtils.toFXImage(bufferedImage, image);
            Platform.runLater(() -> cameraView.setImage(image));
        }
    }

    private Button createPoseButton(String caption, EventHandler<ActionEvent> event) {
        Button btn = new Button(caption);
        btn.setOnAction(event);
        return btn;
    }

    private RadioButton createModeButton(final OperationMode opMode) {
        RadioButton btn = new RadioButton(opMode.getCaption());
        btn.setOnAction(actionEvent -> robotBrain.setOperationMode(opMode));
        btn.setToggleGroup(modeGroup);
        return btn;
    }

    private Node createSliderPane(String text, int min, int max, int init, int majorTickUnit, int minorTickCount,
                                  int blockIncrement, final ServoFunction servo) {
        Slider slider = new Slider(min, max, init);
        slider.setMaxWidth(300);
        slider.setPrefWidth(250);
        slider.setMinWidth(200);

        sliderMap.put(servo, slider);
        slider.valueProperty().addListener((observableValue, oldValue, newValue) -> robotArm.setWantedPosition(servo, newValue.doubleValue() / 100.0));

        slider.setMin(min);
        slider.setMax(max);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(majorTickUnit);
        slider.setMinorTickCount(minorTickCount);
        slider.setBlockIncrement(blockIncrement);

        Label label = new Label(text);
        label.setPrefWidth(100);
        HBox hbox = new HBox();
        hbox.getStyleClass().add("slider-pane");
        hbox.getChildren().addAll(label, slider);
        return hbox;
    }

    public static BufferedImage resize(BufferedImage img, int width, int height) {
        int w = img.getWidth();
        int h = img.getHeight();
        BufferedImage dimg = new BufferedImage(width, height, img.getType());
        Graphics2D g = dimg.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(img, 0, 0, width, height, 0, 0, w, h, null);
        g.dispose();
        return dimg;
    }

    private void setSlidersFromServos() {
        for (final ServoFunction sf : robotArm.getServos()) {
            final Slider slider = sliderMap.get(sf);
            Platform.runLater(() -> slider.setValue(Math.round(robotArm.getCurrentPosition(sf) * 100)));
        }
    }
}
