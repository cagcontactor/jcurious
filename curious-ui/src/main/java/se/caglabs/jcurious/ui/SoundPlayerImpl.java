/*
 * Created by Daniel Marell 13-02-07 7:45 AM
 */
package se.caglabs.jcurious.ui;

import javafx.scene.media.AudioClip;
import se.caglabs.jcurious.brain.SoundPlayer;

/**
 * Plays sound clips stored in resources using the JavaFX API.
 */
public class SoundPlayerImpl implements SoundPlayer {
    private AudioClip lonelyClip = new AudioClip(getClass().getResource("/sounds/lonely.wav").toExternalForm());
    private AudioClip trackingClip = new AudioClip(getClass().getResource("/sounds/pulse.wav").toExternalForm());
    private AudioClip searchingClip = new AudioClip(getClass().getResource("/sounds/droid1.wav").toExternalForm());
    private AudioClip manyFacesClip = new AudioClip(getClass().getResource("/sounds/droid3.wav").toExternalForm());

    public void playLonelyClip() {
        lonelyClip.play();
    }

    @Override
    public void playSearchingModeActivatedClip() {
        searchingClip.play();
    }

    @Override
    public void playTrackingModeActivatedClip() {
        trackingClip.play();
    }

    @Override
    public void playManyFacesInViewClip() {
        manyFacesClip.play();
    }
}
