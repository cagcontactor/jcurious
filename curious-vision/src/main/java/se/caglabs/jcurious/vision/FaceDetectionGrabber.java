/*
 * Created by Daniel Marell 13-01-20 12:08 PM
 */
package se.caglabs.jcurious.vision;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_core.cvLoad;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;
import static org.bytedeco.javacpp.opencv_objdetect.cvHaarDetectObjects;

public class FaceDetectionGrabber {
    public static class FaceDetectionGrabberException extends Exception {
        public FaceDetectionGrabberException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private FrameGrabber grabber;
    private opencv_core.CvMemStorage storage;
    private opencv_objdetect.CvHaarClassifierCascade classifier;
    private int cameraNumber;

    public FaceDetectionGrabber(int cameraNumber) {
        this.cameraNumber = cameraNumber;

        // Preload the opencv_objdetect module to work around a known bug.
        Loader.load(opencv_objdetect.class);

        loadHaarCascadeXml();
    }

    private void loadHaarCascadeXml() {
        try {
            // Load the classifier file from Java resources.
            File classifierFile = Loader.extractResource(getClass(), "/haarcascade_frontalface_alt.xml",
                    null, "classifier", ".xml");
            if (classifierFile == null || classifierFile.length() <= 0) {
                throw new IllegalStateException("Could not extract the classifier file from Java resource.");
            }

            // Preload the opencv_objdetect module to work around a known bug.
            Loader.load(opencv_objdetect.class);
            classifier = new opencv_objdetect.CvHaarClassifierCascade(cvLoad(classifierFile.getAbsolutePath()));
            classifierFile.delete();
            if (classifier.isNull()) {
                throw new IOException("Could not load the classifier file.");
            }
        } catch (IOException e) {
            throw new IllegalStateException("Could not extract the classifier file from Java resource.");
        }
    }

    public GrabberResult getGrabberResult() throws FaceDetectionGrabberException {
        try {
            if (grabber == null) {
                // The available FrameGrabber classes include OpenCVFrameGrabber (opencv_highgui),
                // DC1394FrameGrabber, FlyCaptureFrameGrabber, OpenKinectFrameGrabber,
                // PS3EyeFrameGrabber, VideoInputFrameGrabber, and FFmpegFrameGrabber.'
                for (String s : FrameGrabber.list) {
                    System.out.println("s=" + s);
                }
                grabber = new OpenCVFrameGrabber(cameraNumber);
                System.out.println("starting grabber");
                grabber.start();
                System.out.println("grabber started");

                System.out.println("grabber format=" + grabber.getFormat());
                grabber.setImageWidth(400);
                grabber.setImageHeight(280);
                System.out.println("grabber w=" + grabber.getImageWidth() + ",h=" + grabber.getImageHeight());
            }

            // FAQ about IplImage:
            // - For custom raw processing of data, getByteBuffer() returns an NIO direct
            //   buffer wrapped around the memory pointed by imageData, and under Android we can
            //   also use that Buffer with Bitmap.copyPixelsFromBuffer() and copyPixelsToBuffer().
            // - To get a BufferedImage from an IplImage, we may call getBufferedImage().
            // - The createFrom() factory method can construct an IplImage from a BufferedImage.
            // - There are also a few copy*() methods for BufferedImage<->IplImage data transfers.
            IplImage grabbedImage;

            try {
                grabbedImage = grabber.grab();
            } catch (FrameGrabber.Exception e) {
                System.out.println("grab exception:" + e.getMessage());
                return null;
            }
            int width = grabbedImage.width();
            int height = grabbedImage.height();

            IplImage grayImage = IplImage.create(width, height, IPL_DEPTH_8U, 1);

            List<BufferedImage> workingImages = new ArrayList<>();
            workingImages.add(grayImage.getBufferedImage());

            // Objects allocated with a create*() or clone() factory method are automatically released
            // by the garbage collector, but may still be explicitly released by calling release().
            // You shall NOT call cvReleaseImage(), cvReleaseMemStorage(), etc. on objects allocated this way.
            storage = CvMemStorage.create();

            cvClearMemStorage(storage);

            // Let's try to detect some faces! but we need a grayscale image...
            cvCvtColor(grabbedImage, grayImage, CV_BGR2GRAY);
            CvSeq faces = cvHaarDetectObjects(grayImage, classifier, storage,
                    1.1, 3, CV_HAAR_DO_CANNY_PRUNING);
            List<DetectedObject> detectedObjects = new ArrayList<>();
            int total = faces.total();
            for (int i = 0; i < total; i++) {
                CvRect r = new CvRect(cvGetSeqElem(faces, i));
                detectedObjects.add(new DetectedObject(new Rectangle(r.x(), r.y(), r.width(), r.height())));
            }

            workingImages.add(grayImage.getBufferedImage());
            GrabberResult result = new GrabberResult(detectedObjects, grabbedImage.getBufferedImage(), workingImages);
            return result;
        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            throw new FaceDetectionGrabberException("getGrabberResult failed", e);
        }
    }
}