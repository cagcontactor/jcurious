/*
 * Created by Daniel Marell 13-01-27 8:18 PM
 */
package se.caglabs.jcurious.vision;

public class DetectedObject {
    private Rectangle box;

    public DetectedObject(Rectangle box) {
        this.box = box;
    }

    public Rectangle getBox() {
        return box;
    }
}
