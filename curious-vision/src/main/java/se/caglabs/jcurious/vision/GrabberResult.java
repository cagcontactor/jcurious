/*
 * Created by Daniel Marell 13-01-27 8:20 PM
 */
package se.caglabs.jcurious.vision;

import java.awt.image.BufferedImage;
import java.util.List;

public class GrabberResult {
    private List<DetectedObject> detectedObjects;
    private BufferedImage originalImage;
    private List<BufferedImage> workingImages;

    public GrabberResult(List<DetectedObject> detectedObjects, BufferedImage originalImage, List<BufferedImage> workingImages) {
        this.detectedObjects = detectedObjects;
        this.originalImage = originalImage;
        this.workingImages = workingImages;
    }

    public List<DetectedObject> getDetectedObjects() {
        return detectedObjects;
    }

    public BufferedImage getOriginalImage() {
        return originalImage;
    }

    public List<BufferedImage> getWorkingImages() {
        return workingImages;
    }
}
