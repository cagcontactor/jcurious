/*
 * Created by Daniel Marell 2013-01-31 21:53
 */
package se.caglabs.jcurious.brain;

import se.caglabs.jcurious.arm.ArmPose;
import se.caglabs.jcurious.arm.RobotArm;
import se.caglabs.jcurious.arm.ServoFunction;
import se.caglabs.jcurious.vision.DetectedObject;
import se.caglabs.jcurious.vision.Point;
import se.marell.dcommons.time.PassiveTimer;
import se.marell.dcommons.util.Filter;
import se.marell.dcommons.util.FilterSIIR;

import java.util.List;

public class RandomSearchMode extends TickMode {
    private static final double MIN_TURN_LIMIT = -0.5;
    private static final double MAX_TURN_LIMIT = 0.5;
    private static final double MIN_MOVE_DIST = 0.1;

    private double turnPosition;
    private PassiveTimer turnTimer = new PassiveTimer(5000);
    private PassiveTimer newModeTimer = new PassiveTimer(5000);
    private Filter numFaces = new FilterSIIR(0, 10);
    private Filter numFacesMaxMemory = new FilterSIIR(0, 50); // Store max number of faces, higher value is immediate, lowering is slow/filtered
    private SoundController sound;

    public RandomSearchMode(SoundController sound, RobotArm robotArm, int viewPortWidth, int viewPortHeight) {
        super(OperationMode.searching, robotArm, viewPortWidth, viewPortHeight);
        this.sound = sound;
        System.out.println("RandomSearchMode activated");
        robotArm.setPose(ArmPose.cool);
        turnPosition = robotArm.getCurrentPosition(ServoFunction.turn);
        sound.playSearchingModeActivatedClip();
    }

    @Override
    public void tick(Point targetPoint, List<DetectedObject> faces) {
        numFaces.in(faces.size());
        if (faces.size() > numFacesMaxMemory.out()) {
            numFacesMaxMemory.set(faces.size());
            sound.playManyFacesInViewClip();
        } else {
            numFacesMaxMemory.in(faces.size());
        }
        numFaces.in(faces.size());
        //System.out.printf("numFaces=%.1f\n", numFaces.out());
        if (numFaces.out() <= 0.1) {
            sound.playLonelyClip();
        }
        if (numFaces.out() < 0.2) {
            newModeTimer.restart();
        }
        if (newModeTimer.hasExpired()) {
            // At least one face has been present for a while, enable switch to tracking mode
            robotArm.setPose(ArmPose.upright);
            newMode = OperationMode.tracking;
        } else {
            newMode = null;
        }
        if (turnTimer.hasExpired()) {
            turnTimer.restart(getRandomTurnTimeMsec());
            if (numFaces.out() < 0.1) {
                turnToRandomPosition();
                robotArm.setPose(ArmPose.upright);
            }
        }
        if (newModeTimer.getRemainingTime() < 1000) {
//            robotArm.setPose(ArmPose.curious);
            robotArm.setPose(ArmPose.upright);
            System.out.println("pose curious");
        } else if (newModeTimer.getRemainingTime() < 2000) {
            robotArm.setPose(ArmPose.cool);
            System.out.println("pose cool");
        }
        // System.out.printf("turnPosition=%.1f\n", turnPosition);
    }

    private void turnToRandomPosition() {
        double p;
        do {
            p = Math.random() * (MAX_TURN_LIMIT - MIN_TURN_LIMIT) - MIN_MOVE_DIST;
        } while (Math.abs(turnPosition - p) < MIN_MOVE_DIST);
        turnPosition = p;
        robotArm.setWantedPosition(ServoFunction.turn, turnPosition);
    }

    private int getRandomTurnTimeMsec() {
        return (int) (Math.random() * 20000.0 + 10000.0);
    }

    @Override
    public String getInfoString() {
        return String.format("NF: %.1f NFMax: %.1f", numFaces.out(), numFacesMaxMemory.out());
    }
}
