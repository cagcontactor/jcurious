/*
 * Created by Daniel Marell 2013-01-31 22:00
 */
package se.caglabs.jcurious.brain;

import se.caglabs.jcurious.arm.RobotArm;
import se.caglabs.jcurious.arm.ServoFunction;
import se.caglabs.jcurious.vision.DetectedObject;
import se.caglabs.jcurious.vision.Point;
import se.marell.dcommons.time.PassiveTimer;

import java.util.List;

public class TrackingMode extends TickMode {
    private static final double HORIZONTAL_TARGETING_SPEED = 0.1;
    private static final double VERTICAL_TARGETING_SPEED = 0.03;

    private PassiveTimer textFlashTimer = new PassiveTimer(500);
    private boolean textOn;

    public TrackingMode(SoundController sound, RobotArm robotArm, int viewPortWidth, int viewPortHeight) {
        super(OperationMode.tracking, robotArm, viewPortWidth, viewPortHeight);
        System.out.println("TrackingMode activated");
        sound.playTrackingModeActivatedClip();
    }

    @Override
    public void tick(Point targetPoint, List<DetectedObject> faces) {
        if (targetPoint == null) {
            newMode = OperationMode.searching;
            return;
        }
        double horizontalTargetPosition = targetPoint.getX() / (double) viewPortWidth * 2;
        double verticalTargetPosition = targetPoint.getY() / (double) viewPortHeight * 2;

        //System.out.printf("targetPoint=%s target=%.4f %.4f\n", targetPoint.toString(), horizontalTargetPosition, verticalTargetPosition);

        // Follow focused face
        double cx = robotArm.getCurrentPosition(ServoFunction.turn);
        robotArm.setWantedPosition(ServoFunction.turn, cx + horizontalTargetPosition * HORIZONTAL_TARGETING_SPEED);

        double cy = robotArm.getCurrentPosition(ServoFunction.topTilt);
        robotArm.setWantedPosition(ServoFunction.topTilt, cy + -verticalTargetPosition * VERTICAL_TARGETING_SPEED);
    }

    @Override
    public String getInfoString() {
        if (textFlashTimer.hasExpired()) {
            textFlashTimer.restart();
            textOn = !textOn;
        }
        return textOn ? "Tracking" : "";
    }
}
