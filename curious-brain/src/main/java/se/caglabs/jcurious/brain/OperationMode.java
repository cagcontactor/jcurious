/*
 * Created by Daniel Marell 13-01-30 10:01 PM
 */
package se.caglabs.jcurious.brain;

public enum OperationMode {
    searching("Searching", "Search surroundings"),
    tracking("Tracking", "Follow face movements"),
    selfAware("Self aware", "Self aware");

    private String caption;
    private String description;

    private OperationMode(String caption, String description) {
        this.caption = caption;
        this.description = description;
    }

    public String getCaption() {
        return caption;
    }

    public String getDescription() {
        return description;
    }
}
