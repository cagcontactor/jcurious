/*
 * Created by Daniel Marell 13-02-07 8:00 AM
 */
package se.caglabs.jcurious.brain;

import se.marell.dcommons.time.PassiveTimer;

/**
 * The central point for robot sound. Here is is possible to keep track that sounds are not played too often,
 * that sounds do not interfere with another sound.
 */
public class SoundController {
    private PassiveTimer lonelyTimer = new PassiveTimer((long) ((Math.random() * 10.0 + 5.0) * 60 * 1000));
    private PassiveTimer soundTimer = new PassiveTimer(5 * 1000);
    private SoundPlayer player;

    public SoundController(SoundPlayer player) {
        this.player = player;
    }

    public void playLonelyClip() {
        if (!lonelyTimer.hasExpired()) {
            return;
        }
        lonelyTimer.restart();

        if (!soundTimer.hasExpired()) {
            return;
        }
        soundTimer.restart();

        player.playLonelyClip();
    }

    public void playSearchingModeActivatedClip() {
        if (!soundTimer.hasExpired()) {
            return;
        }
        soundTimer.restart();

        player.playSearchingModeActivatedClip();
    }

    public void playTrackingModeActivatedClip() {
        if (!soundTimer.hasExpired()) {
            return;
        }
        soundTimer.restart();

        player.playTrackingModeActivatedClip();
    }

    public void playManyFacesInViewClip() {
        if (!soundTimer.hasExpired()) {
            return;
        }
        soundTimer.restart();

        player.playManyFacesInViewClip();
    }
}
