/*
 * Created by Daniel Marell 13-01-27 8:44 PM
 */
package se.caglabs.jcurious.brain;

import se.caglabs.jcurious.arm.DoublePoint;
import se.caglabs.jcurious.arm.RobotArm;
import se.caglabs.jcurious.vision.DetectedObject;
import se.caglabs.jcurious.vision.Point;
import se.caglabs.jcurious.vision.Rectangle;
import se.marell.dcommons.time.PassiveTimer;
import se.marell.dcommons.util.Filter;
import se.marell.dcommons.util.FilterSIIR;

import java.util.List;

/**
 * The robot brain. Input is a list of detected objects. Output is control signals for the robot arm.
 */
public class RobotBrain {
    private static final int MIN_FOCUS_DIST = 100;
    private static final int MIN_FOCUS_SIZE_DIFF = 100;
    private static final int FACE_FOCUS_TIMEOUT = 1000;
    private RobotArm robotArm;
    private List<DetectedObject> detectedFaces;
    private DetectedObject focusFace;
    private PassiveTimer lostFaceFocusTimer = new PassiveTimer(FACE_FOCUS_TIMEOUT);
    private Filter faceCenterPosXFilter;
    private Filter faceCenterPosYFilter;
    private Filter faceSizeFilter;
    private boolean manualMode;
    private OperationMode opMode;
    private TickMode tickMode;
    private double facePosX;
    private double facePosY;
    private double faceSize;
    private int viewPortWidth;
    private int viewPortHeight;
    private double horizontalTargetPosition;
    private double verticalTargetPosition;
    private SoundController sound;

    public RobotBrain(RobotArm robotArm, SoundPlayer player) {
        this.robotArm = robotArm;
        this.sound = new SoundController(player);
    }

    public void setManualMode(boolean manualMode) {
        this.manualMode = manualMode;
    }

    public boolean isManualMode() {
        return manualMode;
    }

    public OperationMode[] getOperationModes() {
        return OperationMode.values();
    }

    public void setOperationMode(OperationMode opMode) {
        this.opMode = opMode;
    }

    public void tick() {
        if (faceCenterPosXFilter != null) {
            facePosX = faceCenterPosXFilter.out();
            facePosY = faceCenterPosYFilter.out();
            faceSize = faceSizeFilter.out();
        }
        if (manualMode) {
            return;
        }

        if (tickMode == null) {
            tickMode = new RandomSearchMode(sound, robotArm, viewPortWidth, viewPortHeight);
        } else {
            if (opMode == OperationMode.searching && tickMode.getOperationMode() != OperationMode.searching) {
                tickMode = new RandomSearchMode(sound, robotArm, viewPortWidth, viewPortHeight);
            } else if (opMode == OperationMode.tracking && tickMode.getOperationMode() != OperationMode.tracking) {
                tickMode = new TrackingMode(sound, robotArm, viewPortWidth, viewPortHeight);
            } else {
                if (tickMode.getNewOperationMode() == OperationMode.searching) {
                    tickMode = new RandomSearchMode(sound, robotArm, viewPortWidth, viewPortHeight);
                } else if (tickMode.getNewOperationMode() == OperationMode.tracking) {
                    tickMode = new TrackingMode(sound, robotArm, viewPortWidth, viewPortHeight);
                }
            }
        }

        Point targetPoint = getFaceFocusCenterPoint();
        tickMode.tick(targetPoint, detectedFaces);
    }

    public void setDetectedFaces(List<DetectedObject> faces, int viewPortWidth, int viewPortHeight) {
        this.detectedFaces = faces;
        this.viewPortWidth = viewPortWidth;
        this.viewPortHeight = viewPortHeight;
        int maxWidth = 0;
        DetectedObject largestFace = null;
        for (DetectedObject f : faces) {
            if (f.getBox().getWidth() > maxWidth) {
                largestFace = f;
                maxWidth = f.getBox().getWidth();
            }
        }
        if (largestFace != null) {
            Rectangle r = largestFace.getBox();
            double posX = r.getX() + r.getWidth() / 2;
            double posY = r.getY() + r.getHeight() / 2;
            double size = r.getWidth();

            if (focusFace == null) {
                focusFace = largestFace;
                lostFaceFocusTimer.restart();
                faceCenterPosXFilter = createFilter(posX);
                faceCenterPosYFilter = createFilter(posY);
                faceSizeFilter = createFilter(size);
                System.out.println("Gained face focus");
            } else {
//                System.out.printf("fpx=%.0f/%.0f fpy=%.0f/%.0f sz=%.0f/%.0f\n", facePosX, posX, facePosY, posY, faceSize, size);

                if (Math.abs(posX - facePosX) < MIN_FOCUS_DIST &&
                        Math.abs(posY - facePosY) < MIN_FOCUS_DIST &&
                        Math.abs(size - faceSize) < MIN_FOCUS_SIZE_DIFF) {
                    faceCenterPosXFilter.in(posX);
                    faceCenterPosYFilter.in(posY);
                    faceSizeFilter.in(size);
                    lostFaceFocusTimer.restart();
                    horizontalTargetPosition = (faceCenterPosXFilter.out() - viewPortWidth / 2) / (double) viewPortWidth;
                    verticalTargetPosition = (faceCenterPosYFilter.out() - viewPortHeight / 2) / (double) viewPortHeight;
//                    System.out.println("Keeping face focus");
                    return;
                }
            }
        }
        if (lostFaceFocusTimer.hasExpired()) {
            if (focusFace != null) {
                System.out.println("Lost face focus");
                focusFace = null;
                faceCenterPosXFilter = null;
                faceCenterPosYFilter = null;
                faceSizeFilter = null;
                horizontalTargetPosition = 0;
                verticalTargetPosition = 0;
            }
        }
    }

    public DetectedObject getFaceInFocus() {
        return focusFace;
    }

    public DoublePoint getArmControlSignal() {
        return new DoublePoint(horizontalTargetPosition, verticalTargetPosition);
    }

    /**
     * 0,0 is in center, negative is left/down, positive is right/up
     *
     * @return Coordinate of face in focus or null if no face is in focus
     */
    public Point getFaceFocusCenterPoint() {
        if (faceCenterPosXFilter == null) {
            return null;
        }
        int x = (int) Math.round(faceCenterPosXFilter.out()) - viewPortWidth / 2;
        int y = (int) Math.round(faceCenterPosYFilter.out()) - viewPortHeight / 2;
        return new Point(x, y);
    }

    public List<DetectedObject> getAllFaces() {
        return detectedFaces;
    }

    private Filter createFilter(double value) {
        return new FilterSIIR(value, 3);
    }

    public String getInfoString() {
       return tickMode.getInfoString();
    }
}
