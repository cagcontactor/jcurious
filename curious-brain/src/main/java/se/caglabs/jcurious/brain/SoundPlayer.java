/*
 * Created by Daniel Marell 13-02-06 10:40 PM
 */
package se.caglabs.jcurious.brain;

/**
 * An interface to an implementation that can play sound clips.
 */
public interface SoundPlayer {
    public void playLonelyClip();

    public void playSearchingModeActivatedClip();

    public void playTrackingModeActivatedClip();

    public void playManyFacesInViewClip();
}
