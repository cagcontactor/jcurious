/*
 * Created by Daniel Marell 2013-01-31 21:53
 */
package se.caglabs.jcurious.brain;

import se.caglabs.jcurious.arm.RobotArm;
import se.caglabs.jcurious.vision.DetectedObject;
import se.caglabs.jcurious.vision.Point;

import java.util.List;

public abstract class TickMode {
    protected RobotArm robotArm;
    protected int viewPortWidth;
    protected int viewPortHeight;
    protected OperationMode newMode;
    protected OperationMode opMode;

    protected TickMode(OperationMode opMode, RobotArm robotArm, int viewPortWidth, int viewPortHeight) {
        this.opMode = opMode;
        this.robotArm = robotArm;
        this.viewPortWidth = viewPortWidth;
        this.viewPortHeight = viewPortHeight;
    }

    public abstract void tick(Point targetPoint, List<DetectedObject> faces);

    public OperationMode getNewOperationMode() {
        return newMode;
    }

    public OperationMode getOperationMode() {
        return opMode;
    }

    public String getInfoString() {
        return null;
    }
}
