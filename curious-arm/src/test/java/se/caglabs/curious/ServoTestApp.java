/*
 * Created by Daniel Marell 13-01-22 7:34 AM
 */
package se.caglabs.curious;

import se.caglabs.jcurious.arm.AbsolutePositionServo;
import se.caglabs.jcurious.arm.ServoSetter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class ServoTestApp extends JFrame {
    private AbsolutePositionServo servo;
    private JSlider outSlider;
    private int inputSliderValue;

    public ServoTestApp() {
        super("ServoTestApp");

        JSlider inSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 0);
        inSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                inputSliderValue = source.getValue();
            }
        });
        outSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 0);

        servo = new AbsolutePositionServo("s1", new ServoSetter() {
            @Override
            public void setValue(int servoNumber, int value) {
                outSlider.setValue(value);
            }

            @Override
            public void tick() {
            }
        }, 1, 0f, 1f, 0, 255, 0f, false, 10, Integer.MAX_VALUE);

        //Create the slider and its label
        JLabel sliderLabel = new JLabel("Servo position", JLabel.CENTER);
        sliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel vPane = new JPanel(new BorderLayout());
        vPane.add(createSliderPane(inSlider, "In "), BorderLayout.NORTH);
        vPane.add(createSliderPane(outSlider, "Out"), BorderLayout.SOUTH);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(vPane);
    }

    private JPanel createSliderPane(JSlider slider, String text) {
        //Turn on labels at major tick marks.
        slider.setMajorTickSpacing(100);
        slider.setMinorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));

        JPanel hPane = new JPanel();
        hPane.setLayout(new BoxLayout(hPane, BoxLayout.X_AXIS));

        JLabel label = new JLabel(text, JLabel.LEFT);
        label.setAlignmentX(Component.LEFT_ALIGNMENT);

        hPane.add(label);
        hPane.add(slider);

        hPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        return hPane;
    }

    private void tick() {
        servo.setWantedPosition(inputSliderValue / 255f);
        servo.tick();
    }

    public static void main(String[] args) {
        ServoTestApp app = new ServoTestApp();
        app.pack();
        app.setVisible(true);
        while (true) {
            app.tick();
            try {
                Thread.sleep(20);
            } catch (InterruptedException ignore) {
            }
        }
    }
}
