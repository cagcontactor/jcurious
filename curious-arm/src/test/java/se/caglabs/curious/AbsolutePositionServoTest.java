/*
 * Created by Daniel Marell 13-01-22 8:10 PM
 */
package se.caglabs.curious;

import org.junit.Test;
import se.caglabs.jcurious.arm.AbsolutePositionServo;
import se.caglabs.jcurious.arm.ServoSetter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class AbsolutePositionServoTest {

    private ServoSetter servoSetter = new ServoSetter() {
        @Override
        public void setValue(int servoNumber, int value) {
            AbsolutePositionServoTest.this.servoNumber = servoNumber;
            AbsolutePositionServoTest.this.servoValue = value;
        }

        @Override
        public void tick() {
        }
    };

    private int servoValue;
    private int servoNumber;

    @Test
    public void testNominal() throws Exception {
        AbsolutePositionServo servo = new AbsolutePositionServo("s1", servoSetter, 1, 0.0, 1.0, 0, 99,
                0.0, false, 0, Integer.MAX_VALUE);
        servo.tick();
        assertThat(servo.getName(), is("s1"));
        assertThat(servoNumber, is(1));
        assertThat(servoValue, is(0));

        servo.setWantedPosition(0.0);
        servo.tick();
        assertThat(servoValue, is(0));

        servo.setWantedPosition(0.5);
        servo.tick();
        assertThat(servoValue, is(50));

        servo.setWantedPosition(1.0);
        servo.tick();
        assertThat(servoValue, is(99));
    }

    @Test
    public void testOtherFloatRange() throws Exception {
        AbsolutePositionServo servo = new AbsolutePositionServo("s2", servoSetter, 1, -1.0, 1.0, 0, 100,
                0.0, false, 0, Integer.MAX_VALUE);
        servo.tick();
        assertThat(servoValue, is(50));

        servo.setWantedPosition(0.0);
        servo.tick();
        assertThat(servoValue, is(50));

        servo.setWantedPosition(-0.5);
        servo.tick();
        assertThat(servoValue, is(25));

        servo.setWantedPosition(0.5);
        servo.tick();
        assertThat(servoValue, is(75));

        servo.setWantedPosition(1.0);
        servo.tick();
        assertThat(servoValue, is(100));

        servo.setWantedPosition(-1.0);
        servo.tick();
        assertThat(servoValue, is(0));
//        final int ticks = 100;
//        for (int i = 0; i < ticks; ++i) {
//        }
    }

    @Test
    public void testRangeLimit() throws Exception {
        AbsolutePositionServo servo = new AbsolutePositionServo("s1", servoSetter, 1, 0, 1.0, 25, 75,
                0.0, false, 0, Integer.MAX_VALUE);
        servo.tick();
        assertThat(servoValue, is(25));

        servo.setWantedPosition(0.0);
        servo.tick();
        assertThat(servoValue, is(25));

        servo.setWantedPosition(1.0);
        servo.tick();
        assertThat(servoValue, is(75));
    }

    @Test
    public void testRangeLimitReverse() throws Exception {
        AbsolutePositionServo servo = new AbsolutePositionServo("s1", servoSetter, 1, 0.0, 1.0, 25, 75,
                0.0, true, 0, Integer.MAX_VALUE);
        servo.tick();
        assertThat(servoValue, is(75));

        servo.setWantedPosition(0.0);
        servo.tick();
        assertThat(servoValue, is(75));

        servo.setWantedPosition(1.0);
        servo.tick();
        assertThat(servoValue, is(25));
    }

    @Test
    public void testMaxSpeedForward() throws Exception {
        AbsolutePositionServo servo = new AbsolutePositionServo("s1", servoSetter, 1, 0, 1.0, 0, 9,
                0.0, false, 0, 2);
        servo.tick();
        assertThat(servoValue, is(0));

        servo.setWantedPosition(1.0);
        int prev = servoValue;
        for (int i = 0; i < 15; ++i) {
            servo.tick();
            int step = servoValue - prev;
            if (servoValue < 9) {
                assertTrue(step == 1 || step == 2);
            }
            prev = servoValue;
        }
        assertThat(servoValue, is(9));
    }

    @Test
    public void testMaxSpeedBackward() throws Exception {
        AbsolutePositionServo servo = new AbsolutePositionServo("s1", servoSetter, 1, 0, 1.0, 0, 9,
                1.0, false, 0, 2);
        servo.tick();
        assertThat(servoValue, is(9));

        servo.setWantedPosition(0f);
        int prev = servoValue;
        for (int i = 0; i < 15; ++i) {
            servo.tick();
            int step = servoValue - prev;
            if (servoValue > 0) {
                assertTrue(step == -1 || step == -2);
            }
            prev = servoValue;
        }
        assertThat(servoValue, is(0));
    }

    @Test
    public void testFilter() {
        AbsolutePositionServo servo = new AbsolutePositionServo("s1", servoSetter, 1, 0.0, 1.0, 0, 99,
                0.0, false, 10, Integer.MAX_VALUE);
        servo.tick();
        assertThat(servoValue, is(0));

        servo.setWantedPosition(1.0);
        int prev = servoValue;
        for (int i = 0; i < 999; ++i) {
            servo.tick();
            int step = servoValue - prev;
            if (servoValue == 50) {
                assertTrue(step > 0);
            }
            prev = servoValue;
        }
        assertThat(servoValue, is(99));
    }
}
