/*
 * Created by Daniel Marell 13-01-21 8:31 PM
 */
package se.caglabs.jcurious.arm;

public enum ServoFunction {
    topTilt,
    middleTilt,
    baseTilt,
    turn
}
