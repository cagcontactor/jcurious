/*
 * Created by Daniel Marell 13-01-22 10:10 PM
 */
package se.caglabs.jcurious.arm;

public class MiniSccServoSetter implements ServoSetter {
    private MiniScc scc;

    public MiniSccServoSetter(MiniScc scc) {
        this.scc = scc;
    }

    @Override
    public void setValue(int servoNumber, int value) {
        scc.set(servoNumber, value);
    }

    @Override
    public void tick() {
        scc.tick();
    }
}
