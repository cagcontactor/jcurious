/*
 * Created by daniel 2002-okt-21
 */
package se.caglabs.jcurious.arm;

import se.marell.dcommons.util.Filter;
import se.marell.dcommons.util.FilterSIIR;

/**
 * Controls a motor servo with absolute positioning, for example an R/C servo.
 * A servo is identified by a servo number (0..n) and the position is represented by an
 * integer, typically 0..255).
 */
public class AbsolutePositionServo {
    private String name;
    private int servoNumber;
    private double minLimit;
    private double maxLimit;
    private int minServoLimit;
    private int maxServoLimit;
    private int maxSpeed;
    private int speed;
    private double wantedPosition;
    private boolean isReversed;
    private ServoSetter servoSetter;
    private Filter filter;
    private int servoValue;

    /**
     * Construct a servo with absolute
     *
     * @param name          Servo name/function.
     * @param servoSetter   Setter for the physical servo
     * @param servoNumber   Servo number (0..n)
     * @param minLimit      Min limit in servo units
     * @param maxLimit      Max limit in servo units
     * @param minPosition   Min position in control units, used to be able to set for example -1 or 0 or -Math.PI
     * @param maxPosition   Max position in control units, used to be able to set for example 1 or Math.PI
     * @param startPosition The initial position, normally 0
     * @param isReversed    Set to true to reverse servo direction
     * @param filterLength  Controls servo acceleration, a higher value=slower acceleration. 1=max acceleration
     * @param maxSpeed      Maximum servo speed where 0="cannot move" and 1="unlimited"
     */
    public AbsolutePositionServo(String name, ServoSetter servoSetter, int servoNumber, double minLimit, double maxLimit,
                                 int minPosition, int maxPosition, double startPosition,
                                 boolean isReversed, int filterLength, int maxSpeed) {
        this.name = name;
        this.servoSetter = servoSetter;
        this.servoNumber = servoNumber;
        this.minLimit = minLimit;
        this.maxLimit = maxLimit;
        this.minServoLimit = minPosition;
        this.maxServoLimit = maxPosition;
        this.isReversed = isReversed;
        this.maxSpeed = maxSpeed;
        this.speed = maxSpeed;
        wantedPosition = startPosition;
        if (filterLength > 0) {
            filter = new FilterSIIR(wantedPosition, filterLength);
        } else {
            filter = new Filter(wantedPosition);
        }
        internalTick(true);
    }

    public String getName() {
        return name;
    }

    public double getCurrentPosition() {
        // Todo implement
        /*
        The reverse of this:
        double value = (wantedPosition - vmin) / (vmax - vmin);
        double filteredValue = filter.inout(value);
        int newServoValue = (int) Math.round(filteredValue * (maxServoLimit - minServoLimit) + minServoLimit);
        */
        // Something like:
        //double v = servoValue - minServoLimit / (maxServoLimit - minServoLimit);
        return wantedPosition;
    }

    public void setWantedPosition(double position) {
        // Adjust limits
        if (position > maxLimit) {
            position = maxLimit;
        } else if (position < minLimit) {
            position = minLimit;
        }
        wantedPosition = position;
    }

    public void setWantedPosition(double position, double speed) {
        setWantedPosition(position);
        setSpeed(speed);
    }

    /**
     * Sets the wanted position without limits for speed nor acceleration.
     * @param position
     */
    public void setWantedPositionDirect(double position) {
        setWantedPosition(position);
        internalTick(true);
    }

    public void setSpeed(double speed) {
        this.speed = (int) Math.round(maxSpeed * speed);
    }

    public double getSpeed() {
        return speed;
    }

    public double getWantedPosition() {
        return wantedPosition;
    }

    public double getResolution() {
        return 0.1; //todo
    }

    public void setPower(boolean on) {
    }

    public double getMinPosition() {
        return minLimit;
    }

    public double getMaxPosition() {
        return maxLimit;
    }

    public void tick() {
        internalTick(false);
    }

    private void internalTick(boolean direct) {
        double vmin;
        double vmax;
        if (isReversed) {
            vmin = maxLimit;
            vmax = minLimit;
        } else {
            vmin = minLimit;
            vmax = maxLimit;
        }
        double value = (wantedPosition - vmin) / (vmax - vmin);
        double filteredValue;
        if (direct) {
            filteredValue = value;
        } else {
            filteredValue = filter.inout(value);
        }
        int newServoValue = (int) Math.round(filteredValue * (maxServoLimit - minServoLimit) + minServoLimit);
        if (!direct) {
            if (newServoValue - servoValue > speed) {
                servoValue += speed;
            } else if (newServoValue - servoValue < -speed) {
                servoValue += -speed;
            } else {
                servoValue = newServoValue;
            }
        } else {
            servoValue = newServoValue;
        }
        servoSetter.setValue(servoNumber, servoValue);
    }
}
