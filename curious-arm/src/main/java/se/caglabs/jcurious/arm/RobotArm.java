/*
 * Created by daniel 2002-okt-21 22:40:28
 */
package se.caglabs.jcurious.arm;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the robot arm.
 */
public class RobotArm {
    private static ServoSetter servoSetter;
    private Map<ServoFunction, AbsolutePositionServo> servoMap = new HashMap<>();

    public RobotArm(String portName) {
        if (portName != null) {
            PortInitializer.init();
            try {
                MiniScc scc = new MiniScc(portName);
                servoSetter = new MiniSccServoSetter(scc);
            } catch (IOException e) {
                System.out.println("Failed to create MiniSCC:" + e.getMessage());
            }
        } else {
            servoSetter = new ServoSetter() {
                Integer[] arr = new Integer[8];

                @Override
                public void setValue(int servoNumber, int value) {
                    arr[servoNumber] = value;
                }

                @Override
                public void tick() {
                    //TODO
//                    StringBuilder sb = new StringBuilder();
//                    for (Integer a : arr) {
//                        String value = a == null ? "" : String.format("%3d ", a);
//                        sb.append(value);
//                    }
//                    System.out.println(sb.toString());
                }
            };
        }

        servoMap = new HashMap<ServoFunction, AbsolutePositionServo>() {{
            put(ServoFunction.topTilt,
                    new AbsolutePositionServo("Top tilt", servoSetter, 0, -1f, 1f, 0, 255,
                            0f, false, 2, Integer.MAX_VALUE));
            put(ServoFunction.middleTilt,
                    new AbsolutePositionServo("Middle tilt", servoSetter, 1, 0f, 1f, 10, 150,
                            0.5f, true, 2, Integer.MAX_VALUE));
            put(ServoFunction.baseTilt,
                    new AbsolutePositionServo("Base tilt", servoSetter, 7, 0f, 1f, 40, 165,
                            0.5f, false, 2, Integer.MAX_VALUE));
            put(ServoFunction.turn,
                    new AbsolutePositionServo("Turn", servoSetter, 4, -1f, 1f, 30, 200,
                            0.5f, false, 2, Integer.MAX_VALUE));
        }};
    }

    public synchronized void setWantedPosition(ServoFunction servo, double position) {
        servoMap.get(servo).setWantedPosition(position);
    }

    public synchronized double getCurrentPosition(ServoFunction servo) {
        return servoMap.get(servo).getCurrentPosition();
    }

    public synchronized Collection<ServoFunction> getServos() {
        return servoMap.keySet();
    }

    public synchronized void tick() {
        for (AbsolutePositionServo servo : servoMap.values()) {
            servo.tick();
        }
        servoSetter.tick();
    }

    public void setPose(ArmPose pose) {
        getTopTiltServo().setWantedPosition(pose.getTopTilt());
        getMiddleTiltServo().setWantedPosition(pose.getMiddleTilt());
        getBaseTiltServo().setWantedPosition(pose.getBaseTilt());
    }

    private AbsolutePositionServo getTopTiltServo() {
        return servoMap.get(ServoFunction.topTilt);
    }

    private AbsolutePositionServo getMiddleTiltServo() {
        return servoMap.get(ServoFunction.middleTilt);
    }

    private AbsolutePositionServo getBaseTiltServo() {
        return servoMap.get(ServoFunction.baseTilt);
    }
}
