/*
 * Created by daniel 2002-okt-21
 */
package se.caglabs.jcurious.arm;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class PortInitializer {
  private static List<PortPair> identifiers = new ArrayList<>();

  private static class PortPair {
    public PortPair(CommPortIdentifier id, SerialPort port) {
      this.id = id;
      this.port = port;
    }

    public CommPortIdentifier id;
    public SerialPort port;
  }

  public static void init() {
    identifiers.clear();

    // Get all serial ports in the system
    Enumeration portList = CommPortIdentifier.getPortIdentifiers();

    // Search for portName among those
    System.out.print("PortInitializer:Enumerating serial ports:");
    while (portList.hasMoreElements()) {
      CommPortIdentifier id = (CommPortIdentifier) portList.nextElement();

//      if (!id.getName().startsWith("COM")) {
//        continue;
//      }

      SerialPort port = null;

      // Try to open it - wait max 2 secs
        //System.out.println("Opening " + id.getName());
        try {
        port = (SerialPort) id.open("US", 2000);
      } catch (Exception e) {
        //System.out.println("Failed to open " + id.getName() + ":" + e.getMessage());
      }

      if (port != null) {
        identifiers.add(new PortPair(id, port));
        System.out.print(" " + id.getName());
      }
    }
    System.out.println();
  }

  public static SerialPort getSerialPort(String portName) {
    for (PortPair p : identifiers) {
      if (p.id.getPortType() == CommPortIdentifier.PORT_SERIAL &&
              p.id.getName().equals(portName)) {  // Found portName
        return p.port;
      }
    }
    return null;
  }
}
