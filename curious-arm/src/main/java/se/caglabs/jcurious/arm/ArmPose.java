/*
 * Created by Daniel Marell 2013-01-31 22:37
 */
package se.caglabs.jcurious.arm;

public enum ArmPose {
    cool(0.0, 0.0, 1.0),
    upright(-0.8, 1.0, 0.8),
    curious(0.08, 0.4, 0.5),
    laidBack(-1.0, 0.7, 1.0);

    private double topTilt;
    private double middleTilt;
    private double baseTilt;

    private ArmPose(double topTilt, double middleTilt, double baseTilt) {
        this.topTilt = topTilt;
        this.middleTilt = middleTilt;
        this.baseTilt = baseTilt;
    }

    public double getTopTilt() {
        return topTilt;
    }

    public double getMiddleTilt() {
        return middleTilt;
    }

    public double getBaseTilt() {
        return baseTilt;
    }
}
