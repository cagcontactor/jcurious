/*
 * Created by Daniel Marell 13-01-22 10:08 PM
 */
package se.caglabs.jcurious.arm;

public interface ServoSetter {
    void setValue(int servoNumber, int value);

    void tick();
}
