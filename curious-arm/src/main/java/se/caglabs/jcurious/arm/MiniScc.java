/*
 * Created by daniel 2002-okt-21
 */
package se.caglabs.jcurious.arm;

import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface to a Mini SCC servo controller via serial port.
 * A Mini SCC handles up to 8 servos. A servo position could be 0..255
 * where 128 is centered.
 */
public class MiniScc {
    private OutputStream outputStream;
    private byte[] positions = new byte[8];

    public MiniScc(String portName) throws IOException {
        for (int i = 0; i < 8; ++i) {
            positions[i] = 127;
        }
        SerialPort serialPort = PortInitializer.getSerialPort(portName);
        if (serialPort == null) {
            throw new IOException("Port " + portName + " not found");
        }
        System.out.println("Opened serial port: " + portName);

        // Setup comm parameters
        try {
            serialPort.setSerialPortParams(9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException e) {
            throw new IOException("Illegal hard coded port params");
        }

        // Init outputStream
        try {
            outputStream = serialPort.getOutputStream();
        } catch (IOException e) {
            throw new IOException("Failed to get outputStream");
        }
    }

    public synchronized void set(int servonr, int position) {
        positions[servonr] = (byte) position;
    }

    public synchronized boolean tick() {
        for (int i = 0; i < 8; ++i) {
            byte[] ba = new byte[3];
            ba[0] = -1; // 255
            ba[1] = (byte) i;
            ba[2] = positions[i];
            try {
                outputStream.write(ba);
            } catch (IOException e) {
                System.out.println("Failed to write to miniscc:" + e.getMessage());
            }
        }

        //TODO
//        StringBuilder sb = new StringBuilder();
//        for (byte a : positions) {
//            String value = String.format("%3d ", a);
//            sb.append(value);
//        }
//        System.out.println(sb.toString());

        return true;
    }
}
